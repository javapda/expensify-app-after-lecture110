// https://redux.js.org/
import { createStore } from 'redux';

console.log('redux 101');


// Action generators 
// - are functions that return action objects
const incrementCount = ({ incrementBy = 1 } = {}) => ({
    type: 'INCREMENT',
    incrementBy
});
const decrementCount = ({ decrementBy = 1 } = {}) => ({
    type: 'DECREMENT',
    decrementBy
});
const resetCount = () => ({
    type: 'RESET',
    count: 0
});
const setCount = ({ count }) => ({
    type: 'SET',
    count
});
// Reducers
// 1. Reducers are pure functions
// 2. Never change state or action

const countReducter = (state = { count: 0 }, action) => {
    console.log("store call : ", action + ", count=" + state.count);

    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.incrementBy
            };
        case 'DECREMENT':
            return {
                count: state.count - action.decrementBy
            };
        case 'RESET':
            return {
                count: 0
            };
        case 'SET':
            return {
                count: action.count
            };

        default:
            return state;
    }
};
const store = createStore(countReducter);
const unsubscribe = store.subscribe(() => {
    console.log("END STATE:" + JSON.stringify(store.getState()));
});
// unsubscribe();

store.dispatch(incrementCount({ incrementBy: 5 }));
store.dispatch(resetCount());
store.dispatch(decrementCount({ decrementBy: 10 }));
store.dispatch(setCount({ count: 2234 }));
store.dispatch(setCount({ count: 3 }));

