import React from 'react';
import { Link } from 'react-router-dom';

// Export a stateless functional component
// description, amount, createdAt value

const ExpenseListItem = ({ id, description, amount, createdAt }) => (
    <div>
      <Link to={`/edit/${id}`}>
        <h3>{description}</h3>
      </Link>
      <p>{amount} - {createdAt}</p>
    </div>
  );

// the following gives you access to the dispatch prop
export default ExpenseListItem;