import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
// need css for SingleDatePicker
import 'react-dates/lib/css/_datepicker.css';
import { ANCHOR_LEFT, ANCHOR_RIGHT, HORIZONTAL_ORIENTATION, VERTICAL_ORIENTATION } from 'react-dates/constants';

export default class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: props.expense ? props.expense.description : '',
            note: props.expense ? props.expense.note : '',
            amount: props.expense ? (props.expense.amount / 100).toString() : '',
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calendarFocused: false,
            error: ''
        };
    }

    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };
    onNoteChange = (e) => {
        console.log('on note CHANGE');
        const note = e.target.value;
        this.setState(() => ({ note }));
    };
    onAmountChange = (e) => {
        const amount = e.target.value;
        // https://regex101.com/r/LSrxPz/2
        // ^\d(\.\d{0,2})?
        // require 1 to infinite # before the decimal
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            console.log('setting amount');
            this.setState(() => ({
                amount
            }));
        }
    };
    onDateChange = (createdAt) => {
        console.log(createdAt ? createdAt.format("MMM D, YYYY") : "OUCH createdAt is null");
        if (createdAt) {
            this.setState(() => ({ createdAt }));
        }
    };
    onFocusChange = ({ focused }) => {
        console.log('onFocusChange, focused=' + focused);
        this.setState(() => ({ calendarFocused: focused }));
    };
    onSubmit = (e) => {
        e.preventDefault();
        console.log('submit attempted, description=' + this.state.description + ', amount=' + this.state.amount);
        if (!this.state.description || !this.state.amount) {
            // set error state - please provide description and amount
            console.log('error - please provide description and amount');
            this.setState(() => ({ error: 'Please provide description and amount' }));
        } else {
            // clear the error
            console.log('clearing error');
            this.setState(() => ({ error: '' }));
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.amount.valueOf(),
                note: this.state.note.trim()
            });
        }
    };
    render() {
        return (
            <div>
                Expense Form in ExpenseForm.js
                <form onSubmit={this.onSubmit}>
                    {this.state.error && <div>{this.state.error}</div>}
                    <input
                        type='text'
                        placeholder='Description'
                        autoFocus
                        value={this.state.description}
                        onChange={this.onDescriptionChange}
                        name='description'
                    />
                    <input
                        type='text'
                        placeholder='Amount'
                        value={this.state.amount}
                        onChange={this.onAmountChange}
                        name='amount'
                    />
                    <SingleDatePicker
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange}
                        focused={this.state.calendarFocused}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        // make all days available
                        isOutsideRange={() => false}
                        disabled={false}
                        keepOpenOnDateSelect={false}
                        hideKeyboardShortcutsPanel={false}
                        orientation={HORIZONTAL_ORIENTATION}
                        anchorDirection={ANCHOR_LEFT}
                    />
                    <textarea
                        placeholder='Add a note for your expense (optional).'
                        value={this.state.note}
                        onChange={this.onNoteChange}
                    >
                    </textarea>
                    <button>Add Expense</button>
                </form>
            </div>
        );
    }
}